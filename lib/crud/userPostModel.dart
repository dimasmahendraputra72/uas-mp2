import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.title,
    this.price,
    this.image,
    this.description,
  });

  String title;
  String price;
  String image, description;

  Map toJson() => {
        "title": title,
        "price": price,
        "image": image,
        "description": description,
      };
}
