import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:showroom_motor/crud/userPostModel.dart';
import 'package:showroom_motor/crud/userViewModel.dart';

import '../providers/products.dart';
import '../providers/product.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlFocusNode = FocusNode();

  final _imageUrlController = TextEditingController();
  final _priceController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _titleController = TextEditingController();

  final _form = GlobalKey<FormState>();

  // var _editedProduct = Product(
  //   id: null,
  //   title: '',
  //   price: 0,
  //   description: '',
  //   imageUrl: '',
  // );

  // var isInit = true;

  // var _initValues = {
  //   'title': '',
  //   'description': '',
  //   'price': '',
  //   'imageUrl': '',
  // };

  // var _isLoading = false;

  // @override
  // void initState() {
  //   _imageUrlFocusNode.addListener(_updateImageUrl);

  //   super.initState();
  // }

  // @override
  // void didChangeDependencies() {
  //   if (isInit) {
  //     final productId = ModalRoute.of(context).settings.arguments as String;
  //     if (productId != null) {
  //       _editedProduct =
  //           Provider.of<Products>(context, listen: false).findById(productId);
  //       _initValues = {
  //         'title': _editedProduct.title,
  //         'description': _editedProduct.description,
  //         'price': _editedProduct.price.toString(),
  //         'imageUrl': ''
  //       };
  //       _imageUrlController.text = _editedProduct.imageUrl;
  //     }
  //   }
  //   isInit = false;

  //   super.didChangeDependencies();
  // }

  // @override
  // void dispose() {
  //   _priceFocusNode.dispose();
  //   _descriptionFocusNode.dispose();
  //   _imageUrlController.dispose();
  //   _imageUrlFocusNode.removeListener(_updateImageUrl);
  //   _imageUrlFocusNode.dispose();
  //   super.dispose();
  // }

  // void _updateImageUrl() {
  //   if (!_imageUrlFocusNode.hasFocus) {
  //     setState(() {});
  //   }
  // }

  void _saveForm() async {
    UserpostModel commRequest = UserpostModel();
    commRequest.title = _titleController.text;
    commRequest.price = _priceController.text;
    commRequest.image = _imageUrlController.text;
    commRequest.description = _descriptionController.text;

    UserViewModel()
        .postUser(userpostModelToJson(commRequest))
        .then((value) => print('success'));
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('An error occured!'),
        content: Text(""),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(ctx).pop(true);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Form(
        key: _form,
        child: ListView(
          children: <Widget>[
            TextFormField(
              // initialValue: _initValues['title'],
              decoration: InputDecoration(labelText: 'Title'),
              textInputAction: TextInputAction.next,
              controller: _titleController,
              onFieldSubmitted: (value) {
                FocusScope.of(context).requestFocus(_priceFocusNode);
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please provide a value';
                }
                return null;
              },
            ),
            TextFormField(
              // initialValue: _initValues['price'],
              decoration: InputDecoration(labelText: 'Price'),
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.number,
              focusNode: _priceFocusNode,
              onFieldSubmitted: (value) {
                FocusScope.of(context).requestFocus(_descriptionFocusNode);
              },
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter a price';
                }
                if (double.tryParse(value) == null) {
                  return 'Please enter a valid number';
                }
                if (double.parse(value) <= 0) {
                  return 'Please enter a number greater than zero';
                }
                return null;
              },
              controller: _priceController,
            ),
            TextFormField(
              // initialValue: _initValues['description'],
              decoration: InputDecoration(labelText: 'Description'),
              maxLines: 3,
              keyboardType: TextInputType.multiline,
              focusNode: _descriptionFocusNode,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter a description';
                }
                if (value.length < 10) {
                  return 'Should be atleast 10 characters long';
                }
                return null;
              },
              controller: _descriptionController,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  margin: EdgeInsets.only(top: 8, right: 10),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                  ),
                  child: _imageUrlController.text.isEmpty
                      ? Text('Enter a URL')
                      : FittedBox(
                          child: Image.network(
                            _imageUrlController.text,
                            fit: BoxFit.cover,
                          ),
                        ),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  child: TextFormField(
                    decoration: InputDecoration(labelText: 'Image URL'),
                    keyboardType: TextInputType.url,
                    textInputAction: TextInputAction.done,
                    controller: _imageUrlController,
                    focusNode: _imageUrlFocusNode,
                    onFieldSubmitted: (value) {
                      _saveForm();
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter an image URL';
                      }
                      if (!value.startsWith('https') &&
                          !value.startsWith('http')) {
                        return 'Please enter a valid URL';
                      }
                      if (!value.endsWith('.png') &&
                          !value.endsWith('.jpg') &&
                          !value.endsWith('.jpeg')) {
                        return 'Please enter a valid image URL';
                      }
                      return null;
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
